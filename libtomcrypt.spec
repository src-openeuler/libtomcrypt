Name:            libtomcrypt
Version:         1.18.2
Release:         6
Summary:         A comprehensive, portable cryptographic toolkit
License:         Public Domain or WTFPL
URL:             http://www.libtom.net/
Source0:         https://github.com/libtom/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz
Patch0:          CVE-2019-17362.patch
Patch1:          backport-fix-missing-mutex-unlock.patch
BuildRequires:   libtommath-devel libtool

%description
LibTomCrypt is a fairly comprehensive, modular and portable cryptographic toolkit 
that provides developers with a vast array of well known published block ciphers, 
one-way hash functions, chaining modes, pseudo-random number generators, public 
key cryptography and a plethora of other routines. 

%package devel
Summary:         Development files for libtomcrypt
Requires:        %{name}%{?_isa} = %{version}-%{release}

%description devel
This package contains libraries and header files for developing applications 
using libtomcrypt.

%prep
%autosetup -n %{name}-%{version} -p1

%build
export CFLAGS="%{build_cflags} -DLTM_DESC -DUSE_LTM"
make %{?_smp_mflags} LIBPATH=%{_libdir} INCPATH="%{_includedir}" PREFIX="%{_prefix}" EXTRALIBS="-ltommath" -f makefile.shared

%install
%make_install INSTALL_OPTS="-m 755" INCPATH="%{_includedir}" LIBPATH="%{_libdir}" -f makefile.shared

%delete_la_and_a

%check
make test
./test

%post -n %{name} -p /sbin/ldconfig
%postun -n %{name} -p /sbin/ldconfig

%files
%defattr(-,root,root)
%license LICENSE
%{_libdir}/libtomcrypt.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/*.h
%{_libdir}/libtomcrypt.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Tue Jul 2 2024 yixiangzhike <yixiangzhike007@163.com> - 1.18.2-6
- backport upstream patch to fix missing mutex unlock

* Wed Apr 13 2022 yixiangzhike <yixiangzhike007@163.com> - 1.18.2-5
- enable test suite in check

* Thu Jan 14 2021 openEuler Buildteam <buildteam@openeuler.org> - 1.18.2-4
- fix CVE-2019-17362

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.18.2-3
- modify build path

* Thu Oct 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.18.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify libtomcrypt spec

* Thu Oct 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.18.2-1
- Package init
